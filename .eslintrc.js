module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/essential',
        '@vue/airbnb',
    ],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        indent: ['error', 4],
        'import/no-unresolved': 'off',
        'import/extensions': [
            0,
            'always',
            {
                packages: 'never',
            },
        ],
        'import/no-extraneous-dependencies': 0,
        'no-param-reassign': ['error', {
            'props': false
        }],
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
};
