import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import routerNames from './routeNames';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: routerNames.home,
            component: Home,
        },
        {
            path: '/about',
            name: routerNames.about,
            component: () => import('@/views/About.vue'),
        },
        {
            path: '/contact',
            name: routerNames.contact,
            component: () => import('@/views/Contact.vue'),
        },
    ],
    mode: 'history',
});
