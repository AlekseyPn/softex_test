import Vue from 'vue';
import VeeValidate, { Validator } from 'vee-validate';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;
Vue.use(VeeValidate, {
    events: 'blur',
});
Validator.localize('en');
new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
