import response from './response';

export default {
    post() {
        return Promise.resolve(response);
    },
};
