import Vue from 'vue';
import Vuex from 'vuex';
import http from '@/http-client';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        form: {
            name: '',
            phone: '',
            message: '',
        },
    },
    mutations: {
        setFormData(state, payload) {
            state.form = {
                ...state.form,
                ...payload,
            };
        },
    },
    actions: {
        sendForm({ commit }) {
            return http.post().then((response) => {
                commit('setFormData', {
                    name: '',
                    phone: '',
                    message: '',
                });
                return response;
            });
        },
    },
});
